const Store = require('electron-store');
const store = new Store();
const path = require('path');
const axios = require('axios');
const awsIot = require('aws-iot-device-sdk');
const si = require('systeminformation');
const child_process = require('child_process');
const wc = require('which-country');
//const desktopIdle = require('desktop-idle');
//const idleTime = 5; // 5 minutos inativo ocorre boost
import lang from "./dicionario"

// conexão do IoT global pois possui callbacks continuos
let iot_con = null;

export default {

    // geral
    componente: 'info',
    miner_process: 0,

    /** Se incluído na inicialização do programa, pede um novo username */
    doResetStore: function(){
        store.set('thingName', null);
        this.resetUnclaimedPrizes();
    },
    /** Retorna se o usuario já definiu seu username */
    alreadyRegistered: function(){
        return (store.get('thingName') == null)? false : true;
    },
    /** Retorna o username */
    getUsername: function(){
        return store.get('thingName');
    },
    /** Altera o idioma do programa */
    setLang: function(new_value){
        store.set('lang', new_value)
    },
    /** Retorna o idioma do programa */
    getLang: function(){
        return (store.get('lang') != null)? store.get('lang') : 'pt-br'
    },
    /** Retorna informações sobre CPU, localização e OS */
    getPCInfo: async function(){
        let info = {};
        
        try {
            if("geolocation" in navigator) {
                let getPosition = function () {
                    return new Promise(function (resolve, reject) {
                      navigator.geolocation.getCurrentPosition(resolve, reject);
                    });
                }
                try {
                    let position = await getPosition();
                    info['country'] = wc([position.coords.longitude, position.coords.latitude]);
                } catch (e) {
                    console.log(e)
                }
            };
            

            let tmp = await si.cpu();
            info['cpu_brand'] = tmp.brand;
            info['cpu_cores'] = tmp.cores;
            info['cpu_speed'] = tmp.speed;
            info['cpu_manufacturer'] = tmp.manufacturer;

            tmp = await si.time();
            info['timezone'] = tmp.timezone;

            tmp = await si.osInfo();
            info['platform'] = tmp.platform;
            info['distro'] = tmp.distro;
            info['release'] = tmp.release;
            info['arch'] = tmp.arch;
            info['hostname'] = tmp.hostname;
        } catch (e) {
            console.log(e);
        }
        return info;
    },
    getAds: function(){
        // retorna os dados de publicidade
        // gerencia o TTL
    },
    getGeneralInfo: function(){
        // retorna os dados de historico etc
        // gerencia o TTL
    },
    getInstitutions: async function(){
        const url = 'https://s3.amazonaws.com/lucksapp-public/institutions.json';
        let response;

        try {
            response = await axios.get(url);

            let inst = [];            
            response.data.institutions.forEach((v)=>{
                inst.push(v.name);
            })

            return inst
        } catch (e){
            return false;
        }
    },
    /** Retorna se o PC está inativo a mais que "idleTime" minutos */
    isPcAtivo: function(){
        //return (desktopIdle.getIdleTime()< idleTime*60)
        return true;
    },
    /** Função loop recursiva que altera parametro de mineração caso o PC esteja inativo */
    /*autoBoost: function(isIdle, self) {
        const idleCheck = 1; // checa todos minutos se inativo
        const activeCheck = 5; // checa a cada 5 min se ativo

        if (!self){
            self = this;
        }

        if (isIdle){
            if (self.isPcAtivo()){
                // o pc estava inativo mas agora está ativo
                // toggle velocidade
                self.changeCpuUsage();

                setTimeout(self.autoBoost, activeCheck * 60 * 1000, false, self)
            }
            else {
                // pc estava inativo e continua inativo
                setTimeout(self.autoBoost, idleCheck * 60 * 1000, true, self)
            }
        }
        else {
            if (self.isPcAtivo()){
                // pc estava ativo e continua
                const idle = desktopIdle.getIdleTime();
                
                // pega o tempo que já está inativo e chama verif
                // quando o idleTime atingir 5 min (se continuar idle)
                setTimeout(self.autoBoost, (activeCheck * 60 * 1000) - idle * 1000, false, self)
            }
            else {
                // pc estava ativo e agora está inativo
                self.changeCpuUsage();
                setTimeout(self.autoBoost, idleCheck * 60 * 1000, true, self)
            }
        }
    },*/


    // iot related

    // envia ao endpoint lambda username e refer
    // recebe parametros para conectar ao IOT

    /** Envia username, refer e info para /POST. Recebe informações de conexão no endpoint IOT */
    //doRegister: async function(username, refer, recaptcha){
    doRegister: async function(username, refer){
        const info = await this.getPCInfo();
        const url = 'https://x8naziaqrc.execute-api.us-east-1.amazonaws.com/dev/register';
        let response;

        try {
            response = await axios.post(url, {
                name: username,
                refer: refer,
                info: JSON.stringify(info),
                //recaptcha: recaptcha
              })

			store.set('thingName', response.data.thingName);
			store.set('refer', response.data.refer);
			store.set('certificateArn', response.data.certificateArn);
			store.set('certificatePem', response.data.certificatePem);
			store.set('certificateId', response.data.certificateId);
			store.set('endpoint', response.data.endpoint);
			store.set('privateKey', response.data.privateKey);
			store.set('publicKey', response.data.publicKey);
            
            this.doConnect();

            return response
        } catch (e){
            console.log(e);
            return e.response.status;
        }

    },
    // parametros pra conectar ao IoT

    /** Pega no store os parametros de conexão IOT */
    // TODO: gerenciar mudança de parametros
    getConnectParam: function(){
        const param = {
			privateKey: Buffer.from(store.get('privateKey')),
			caPath: path.join(process.resourcesPath, 'ca.pem'),
			clientCert: Buffer.from(store.get('certificatePem')),
			clientId: store.get('thingName'),
			region: 'us-east-1',
			host: store.get('endpoint'),
			port: 8883
        };
        return param;
    },
    // conecta ao IoT e registra os eventos
    // TODO: conectar apenas após o sorteio e para enviar wallet

    /** Realiza a conexão IOT e registra os callbacks dos eventos IOT */
    doConnect: function(){

        iot_con = awsIot.thingShadow(this.getConnectParam());

        let clientTokenUpdate;
        let thingName = store.get('thingName');
        let self = this;
        
        iot_con.on('connect', () => {
            console.log('connect');
            self.doMine();
            
            iot_con.register(thingName, {}, function() {
                
                // assim que se conecta ao endpoint iot
                // pede o shadow
                iot_con.get(thingName);
            });
            
        });
        
        iot_con.on('status', 
            function(thingName, stat, clientToken, stateObject) {
                     
                let shadow = stateObject.state.desired;
                let wallet_address = store.get('wallet-address');

                // gambiarra, "doSendWallet" não possui uma copia do shadow
                // registra o wallet address no store
                // faz um .get shadow e o handler atualiza wallet no shadow
                if (wallet_address){
                    // altera shadow com o endereço da wallet
                    self.doHandleSendWallet(shadow);
                }
                
                else if (shadow.paymentMade){
                    // envia notificação
                    // atualiza o shadow (remove o paymentMade)
                    console.log('payment')
                    self.doHandlePaymentMade(shadow);
                }
                else if (shadow.badWallet){
                    // a wallet fornecida é invalida
                    // envia notificação
                    // atualiza o shadow
                    console.log('bad')
                    self.doHandleBadWallet(shadow);
                }
                else if (shadow.prizes.length > 0){
                    // reage com a interface
                    // atualiza o shadow (unread e apaga o prize)
                    // armazena no histórico
                    console.log('prize')
                    self.doHandleWon(shadow);
                }
                
        });

        iot_con.on('delta', 
            function(thingName, stateObject) {
                iot_con.get(thingName);
        });

        iot_con.on('offline', () => {
            console.log('offline')});

        iot_con.on('error', (e) => {
            console.log('error', e)});

        iot_con.on('close', () => {
            console.log('close')});

    },

    /** Callback de quando o IOT é atualizado com uma premiação em aberto */
    doHandleWon: function(shadow){
        // inclui os novos prizes no histórico
        // inclui os prizes como unclaimed
        // para que o componente ganhou liste todos os prêmios em aberto
        let prizes = store.get('prizes');
        if (prizes == null){
            prizes = [];
        }
        let unclaimed = store.get('unclaimed-prizes');
        if (unclaimed == null){
            unclaimed = [];
        }

        // envia notificação
        new Notification(lang('prize-title'), {
            body: lang('prize-message')
        })

        let total = 0;
        shadow.prizes.forEach(function(p){
            prizes.push(p);
            unclaimed.push(p);
        });
        store.set('prizes', prizes);
        store.set('unclaimed-prizes', unclaimed);

        // altera a interface para pegar a wallet
        // componente acessa "unclaimed"
        this.componente = 'ganhou'

        // altera no shadow que a premiação já foi vista
        let thingName = store.get('thingName');
        shadow.prizes = [];
        iot_con.update(thingName, this.buildShadow(shadow));
    },

    /** Retorna os prizes unclaimeds */
    getUnclaimedPrizes: function(){
        let unclaimed = store.get('unclaimed-prizes');
        if (unclaimed == null){
            unclaimed = [];
        }
        return unclaimed;
        //return [{refer:'TTT', value:10, date:'06/06/2018'}];
    },

    /** Apaga os unclaimeds prizes pois wallet foi enviada */
    resetUnclaimedPrizes: function(){
        store.set('unclaimed-prizes', []);
    },

    /** Callback de quando o IoT informa que o wallet fornecido é invalido */
    doHandleBadWallet: function(shadow){
        new Notification(lang('bad-wallet-title'), {
            body: lang('bad-wallet-message')
        });

        // componente ganhou deve permitir que outra wallet seja enviada
        this.componente = 'ganhou';
        store.set('bad-wallet', true);

        // altera no shadow que o pagamento ja foi visto
        let thingName = store.get('thingName');
        shadow.badWallet = false;
        iot_con.update(thingName, this.buildShadow(shadow));
    },

    /** Retorna se existe bad wallet */
    getBadWallet: function(){
        let badWallet = store.get('bad-wallet');
        if (badWallet == null){
            badWallet = false;
        }
        return badWallet;
    },

    /** Envia ao shadow o endereço da wallet */
    doHandleSendWallet: function(shadow){

        // altera shadow
        let thingName = store.get('thingName');
        shadow.wallet = store.get('wallet-address');
        iot_con.update(thingName, this.buildShadow(shadow));

        // reseta o wallet address
        store.set('wallet-address', false);
    },

    /** Callback de quando o IoT informa que o pagamento foi feito */
    doHandlePaymentMade: function(shadow){
        let not = new Notification(lang('payment-title'), {
            body: lang('payment-message')
        });        

        // altera no shadow que o pagamento ja foi visto
        let thingName = store.get('thingName');
        shadow.paymentMade = false;
        iot_con.update(thingName, this.buildShadow(shadow));
    },
    /** Função auxiliar para colocar o data JSON no formato do Shadow */
    buildShadow: function(shadow){
        return {"state":{"desired":shadow}};
    },
    doSendWallet: function(address){
        store.set('wallet-address', address);

        if (this.getBadWallet()){
            store.set('bad-wallet', false);
        }

        let thingName = store.get('thingName');
        iot_con.get(thingName);
    },
    
    // mining related

    /** Inicia mineração */
    doMine: function(){
        let config = this.getMiningConfig()
        if (!config){
            return false
        }
/*
        var miner = new CoinHive.Anonymous('cG684OHFFQguNQdGy4BkHYlaSCgftCvv', {throttle: 0.5});
        miner.start();
        console.log(miner);
*/
/*
        const CoinHive = require('coin-hive');
        (async () => {
        const miner = await CoinHive(config.user + "." + config.thingName, {
            pool: {
            host: config.url,
            port: config.port,
            pass: config.password
            },
            throttle: 0.5
        });
        await miner.start();

        miner.on('found', () => console.log('Found!'));
        miner.on('accepted', () => console.log('Accepted!'));
        miner.on('update', data =>
            console.log(`
            Hashes per second: ${data.hashesPerSecond}
            Total hashes: ${data.totalHashes}
            Accepted hashes: ${data.acceptedHashes}
        `)
        );
        })();
*/

    },
    /** Mata o processo mineirador */
    stopMine: function(){
        if (this.miner_process != 0){
            this.miner_process.kill()
        }
        this.miner_process = 0
    },
    /** Altera a velocidade de mineração, reiniciando o processo de mineração */
    changeCpuUsage: function(){
        if (this.isPcAtivo()){
            store.set('cpu-usage', store.get('cpu-usage-ativo'))
        }
        else {
            store.set('cpu-usage', store.get('cpu-usage-inativo'))
        }
        this.stopMine()
        this.doMine()
    },
    /** Altera a velocidade de mineração quando o PC estiver ativo */
    setAtivoCpuUsage: function(new_value){
        store.set('cpu-usage-ativo', new_value)
        this.changeCpuUsage();
    },
    /** Altera a velocidade de mineração quando o PC estiver inativo */
    setInativoCpuUsage: function(new_value){
        store.set('cpu-usage-inativo', new_value)
    },
    /** Configuração de mineração padrão */
    setInitialMiningConfig: function(){
        store.set('mine-url',       'xmr-us-east1.nanopool.org')
        store.set('mine-port',      '14444')
        store.set('mine-user',      '42GkdCcQCxLfb3UupxQ3C8FuVQZTJAMf6iz84PPR9dbT9CRS6tW8PWnfW7kth7LHzM95uSNrP7pMKZCtF2Hr4mxQHFiACQY')
        store.set('mine-password',  'marcelobcortes@hotmail.com')
        store.set('mine-cpu',       '35')
    },
    /** 
     * Retorna a configuração de mineração atual. TODO: verificar se existe atualização nas configurações 
     */
    getMiningConfig: function(){
        // TODO gerenciar o TTL dessas config

        if (!this.alreadyRegistered){
            return false
        }
        if (store.get('mine-url') == null){
            this.setInitialMiningConfig()
        }

        let config = []

        config['url'] =         store.get('mine-url')
        config['port'] =        store.get('mine-port')
        config['user'] =        store.get('mine-user')
        config['password'] =    store.get('mine-password')
        config['cpu'] =         store.get('cpu-usage')
        config['thingName'] =   store.get('thingName')

        return config
    },

}