const Store = require('electron-store');
const store = new Store();
const user_lang = (store.get('lang') != null)? store.get('lang') : 'pt-br';

const dictionary = [];

dictionary['pt-br'] = {
    'username_placeholder':         'Escolha um nome de usuário!',
    'refer_placeholder':            'Quem te indicou?',
    'instituicoes_placeholder':     'Ou doe a indicação!',
    'registrar':                    'Registrar!',
    'validacao_branco':             'Campo não pode ficar em branco.',
    'validacao_letras':             'Apenas letras, números e traço.',
    'validacao_length':             'Mínimo 3 e máximo 30 caracteres.',
    'validacao_igual':              'O indicador não pode ser você mesmo.',
    'recaptcha_branco':             'Complete o recaptcha.',
    'confirm_register':             function(arg){ return 'Confirma registrar usuário ' + arg[0] + ' e indicador ' + arg[1] + '?'},

    'prize-title':                  'Parabéns!!!',
    'prize-message':                'Você ganhou uma premiação!',
    'data-venceu':                  function(arg){ return 'Na data ' + arg },
    'text-venceu':                  'Você foi sorteado e recebe:',
    'text-venceu-refer':            function(arg){ return 'Seu indicado ' + arg + ' venceu e você recebe:'},

    'payment-title':                'Pagamento efetuado!',
    'payment-message':              'Confira sua wallet. Informações no site.',
    
    'bad-wallet-title':             'Erro no pagamento!',
    'bad-wallet-message':           'O endereço fornecido é inválido.',
    'bad-wallet-text':              'O endereço inserido era inválido. Forneça um endereço válido.',

    'payment-wallet-text':          'Insira um endereço de pagamento.',
    'payment-wallet-placeholder':   'NANO Wallet',
    'payment-wallet-send':          'Registrar endereço',
    'payment-wallet-sent':          'Endereço enviado!',

    'pc-ativo':                     'Computador ativo',
    'pc-inativo':                   'Computador inativo',
    'silence-mode':                 'Modo silencioso',
    'silence-text':                 'Notificação por e-mail e pagamento automático.',
    'silence-email':                'E-mail de notificação',
    'silence-wallet':               'Wallet para recebimento.',
    'silence-activate':             'Ativar!',

    'bem-vindo':                    'Bem-vindo',
}

export default function lang(text, arg){
    if (arg)
        return dictionary[user_lang][text](arg);
    return dictionary[user_lang][text];
    
}