import { app, BrowserWindow, ipcMain, Tray, Menu, protocol, powerSaveBlocker } from 'electron'
const path = require('path');
const fs = require('fs')
const {autoUpdater} = require("electron-updater");
var AutoLaunch = require('auto-launch');
var service = require ("os-service");

process.env.GOOGLE_API_KEY = 'AIzaSyB6cc4x4gYxyRAt6QJyNV9HMcaKomsgVQ4'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
let tray
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

// when receiving a quitAndInstall signal, quit and install the new version ;)
ipcMain.on("quitAndInstall", (event, arg) => {
    autoUpdater.quitAndInstall();
})

function createTray() {

  const iconPath = process.env.NODE_ENV === 'development'
  ? path.join(__dirname,'icon.png')
  : path.join(process.resourcesPath, 'icon.png')

	tray = new Tray(iconPath);

	var trayContextMenu = Menu.buildFromTemplate([
		{label: 'Show/Hide',
		click: function() {
			if (mainWindow.isVisible()){
				mainWindow.hide();
				return;
			}
			mainWindow.show();
			mainWindow.setAlwaysOnTop(true);
			mainWindow.focus();
			mainWindow.setAlwaysOnTop(false);
		}},

		{label: 'Quit', click: function() {
			app.quit();
		}}
	]);
	tray.setContextMenu(trayContextMenu);
	
	return tray;
}

function createWindow (e) {
  /**
   * Initial window options
   */

  
  const id = powerSaveBlocker.start('prevent-display-sleep')

    tray = createTray();

    let autoLaunch = new AutoLaunch({
      name: 'Lucksapp',
      path: app.getPath('exe'),
      isHidden: true
    });
    autoLaunch.isEnabled().then((isEnabled) => {
      if (!isEnabled) autoLaunch.enable();
    });

    autoUpdater.checkForUpdates();
  

  if (process.argv[1] == '--hidden'){
    mainWindow = new BrowserWindow({
      height: 500,
      useContentSize: true,
      width: 400,
      show: false,
      webPreferences: {webSecurity: false}
    })
  }
  else {
    mainWindow = new BrowserWindow({
      height: 500,
      useContentSize: true,
      width: 400,
      webPreferences: {webSecurity: false}
    })
  }
  /*
  protocol.registerBufferProtocol('marcelo', (request, callback) => {
    console.log(request);
    callback({data: fs.readFileSync(request.url.slice(10))})
  }, (error) => {
    if (error) console.error('Failed to register protocol')
  })*//*
  if (!process.env.NODE_ENV === 'development'){
    protocol.registerHttpProtocol('http', function(request, callback) {

      //console.log(request)
      //console.log(callback)
      //console.log(request.url.replace("http:", "file:/"))

      //mainWindow.loadURL(request.url.replace("http:", "file:/"))

      callback({ 'url' : request.url.replace("https:", "file:/"), 'method': request.method});
    });
  }
/*
  protocol.interceptFileProtocol('file', function(request, callback) {

    console.log(request)
    console.log(callback)

    //callback({ 'url' : request.url.replace("https", "file"), 'method': request.method, 'referer': request.referrer, 'session' : null });
  });*/


  mainWindow.loadURL(winURL)
  mainWindow.webContents.openDevTools()


  mainWindow.on('close', (e) => {
    if (mainWindow.isVisible()){
      e.preventDefault();
      mainWindow.hide();
      return;
    }
    mainWindow = null;
    tray.destroy();	
    tray = null;
  })

}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
